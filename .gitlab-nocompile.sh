#!/bin/bash

# This script is embedded as an include into the master .gitlab-fpkg.sh script
# so do not use "exit" unless an error occurs. If variables are needed to
# compile, I highly recommend using them in a function as local.

function compile_project () {
	divider 'assemble executables'
	pushd 'SOURCE/V8POWER' || return 1
	chmod +x ./MKV8.SH || return 1
	./MKV8.SH || return 1
	divider 'upx compresss executables'
	upx --8086 BIN/*.COM  # ignore upx errors
	mv -f BIN/*.COM ../../BIN || return 1
	rmdir BIN || return 1
	popd || return 1
}

apt-get install -y nasm upx || exit $?
compile_project || exit $?


# do not use exit